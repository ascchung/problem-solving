def sockMerchant(n, ar):
    # Pile of socks to be paired by color
    # Each color = #
    # How many pairs of socks with matching colors?

    # Initialize a dictionary to store count of each color
    sock_count = {}
    pairs = 0

    # Count each sock color
    for color in ar:
        # If color does not match
        if color not in sock_count:
            sock_count[color] = 0
        sock_count[color] += 1

        # Check for pairs
        if sock_count[color] % 2 == 0:
            # Increment pair count
            pairs += 1

    # Return # of pairs
    return pairs
