#!/bin/python3

import math
import os
import random
import re
import sys

#
# Complete the 'marsExploration' function below.
#
# The function is expected to return an INTEGER.
# The function accepts STRING s as parameter.
#

def marsExploration(s):
    # initialize count to 0
    count = 0
    # define pattern
    pattern = "SOS"
    # go through the length of string range

    #SOLUTION 1
    # for i in range(len(s)):
    #     # if character in the string does not match the pattern
    #     if s[i] != pattern[i % 3]:
    #         # increment count
    #         count += 1
    # return count

    #SOLUTION 2
    for x in range(0, len(s), 3):
        if s[x] != "S":
            count += 1
        if s[x + 1] != "O":
            count += 1
        if s[x + 2] != "S":
            count += 1
    return count






if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    s = input()

    result = marsExploration(s)

    fptr.write(str(result) + '\n')

    fptr.close()
