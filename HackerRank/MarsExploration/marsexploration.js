"use strict";

const fs = require("fs");

process.stdin.resume();
process.stdin.setEncoding("utf-8");

let inputString = "";
let currentLine = 0;

process.stdin.on("data", function (inputStdin) {
  inputString += inputStdin;
});

process.stdin.on("end", function () {
  inputString = inputString.split("\n");

  main();
});

function readLine() {
  return inputString[currentLine++];
}

/*
 * Complete the 'marsExploration' function below.
 *
 * The function is expected to return an INTEGER.
 * The function accepts STRING s as parameter.
 */

function marsExploration(s) {
  // initialize count to 0
  let count = 0;
  // for loop length of string range
  // if the indexes do not equal to string
  // add to count
  // return count
  for (let i = 0; i < s.length; i += 3) {
    if (s[i] !== "S") {
      count++;
    }
    if (s[i + 1] !== "O") {
      count++;
    }
    if (s[i + 2] !== "S") {
      count++;
    }
  }
  return count;
}

function main() {
  const ws = fs.createWriteStream(process.env.OUTPUT_PATH);

  const s = readLine();

  const result = marsExploration(s);

  ws.write(result + "\n");

  ws.end();
}
