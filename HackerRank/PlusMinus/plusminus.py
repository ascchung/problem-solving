#!/bin/python3

import math
import os
import random
import re
import sys

#
# Complete the 'plusMinus' function below.
#
# The function accepts INTEGER_ARRAY arr as parameter.
#

def plusMinus(arr):
    # define length
    length = len(arr)
    # define positive, negative, zero
    positive = 0
    negative = 0
    zero = 0
    # run through each integer
    for i in arr:
        if i > 0:
            positive += 1
        elif i < 0:
            negative += 1
        else:
            zero += 1
    print("%.6f" % (positive / length))
    print("%.6f" % (negative / length))
    print("%.6f" % (zero / length))



if __name__ == '__main__':
    n = int(input().strip())

    arr = list(map(int, input().rstrip().split()))

    plusMinus(arr)
