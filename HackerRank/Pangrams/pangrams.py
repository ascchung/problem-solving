#!/bin/python3

import math
import os
import random
import re
import sys

#
# Complete the 'pangrams' function below.
#
# The function is expected to return a STRING.
# The function accepts STRING s as parameter.
#

def pangrams(s):
    # all letters must be present in the string
    alphabet = "abcdefghijklmnopqrstuvwxyz"
    # lowercase the string
    lower_s = s.lower()
    # need to create a condition of not pangram to avoid loop from ending once first letter is found
    for letter in alphabet:
        # if one letter does not exist in the pangram, automatically returns "not pangram"
        if letter not in lower_s:
            return "not pangram"
    # otherwise return pangram outside of the for loop
    return "pangram"

s = "We promptly judged antique ivory buckles for the prize"
result = pangrams(s)

print(result)

if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    s = input()

    result = pangrams(s)

    fptr.write(result + '\n')

    fptr.close()
