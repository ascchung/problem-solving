#
# Complete the 'findMedian' function below.
#
# The function is expected to return an INTEGER.
# The function accepts INTEGER_ARRAY arr as parameter.
#
def findMedian(arr):
    # sort the list
    arr.sort()
    # find the middle index
    median = len(arr) // 2
    # divide the length of the array by 2
    return arr[median]
    # return the middle element
