#!/bin/python3

import math
import os
import random
import re
import sys

#
# Complete the 'miniMaxSum' function below.
#
# The function accepts INTEGER_ARRAY arr as parameter.
#

def miniMaxSum(arr):
    # define min and max
    minimum = min(arr)
    maximum = max(arr)
    # subtract the sum of arr to min and max
    min_sum = sum(arr) - minimum
    max_sum = sum(arr) - maximum
    # print min max
    print(max_sum, min_sum)

if __name__ == '__main__':

    arr = list(map(int, input().rstrip().split()))

    miniMaxSum(arr)
