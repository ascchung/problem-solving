#!/bin/python3

import math
import os
import random
import re
import sys

#
# Complete the 'countingValleys' function below.
#
# The function is expected to return an INTEGER.
# The function accepts following parameters:
#  1. INTEGER steps
#  2. STRING path
#

def countingValleys(steps, path):
    # initialize altitude to 0 (sea level)
    altitude = 0
    # initialize a valley count to 0
    valley_count = 0
    # iterate through string of steps and update altitude based on each step
    for step in path:
        if step == "D":
            altitude -= 1
        elif step == "U":
            altitude += 1
    # determine if step is sea level or below, valley ends
        if altitude == 0 and step == "U":
            valley_count += 1
    return valley_count


if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    steps = int(input().strip())

    path = input()

    result = countingValleys(steps, path)

    fptr.write(str(result) + '\n')

    fptr.close()
