'use strict';

const fs = require('fs');

process.stdin.resume();
process.stdin.setEncoding('utf-8');

let inputString = '';
let currentLine = 0;

process.stdin.on('data', function(inputStdin) {
    inputString += inputStdin;
});

process.stdin.on('end', function() {
    inputString = inputString.split('\n');

    main();
});

function readLine() {
    return inputString[currentLine++];
}

/*
 * Complete the 'birthday' function below.
 *
 * The function is expected to return an INTEGER.
 * The function accepts following parameters:
 *  1. INTEGER_ARRAY s
 *  2. INTEGER d
 *  3. INTEGER m
 */

function birthday(s, d, m) {
    let count = 0;

    // Edge case: If m is greater than the length of s, no valid segments exist
    if (m > s.length) {
        return 0;
    }

    // Initialize sum for the first segment of length m
    let currentSum = 0;
    for (let i = 0; i < m; i++) {
        currentSum += s[i];
    }

    // Debug output
    console.log(`Initial currentSum: ${currentSum}`);

    // Check the first segment
    if (currentSum === d) {
        count++;
    }

    // Slide the window across the chocolate bar to calculate sums
    for (let i = 1; i <= s.length - m; i++) {
        currentSum = currentSum - s[i - 1] + s[i + m - 1];
        // Debug output
        console.log(`Current currentSum: ${currentSum}`);
        if (currentSum === d) {
            count++;
        }
    }

    return count;
}


function main() {
    const ws = fs.createWriteStream(process.env.OUTPUT_PATH);

    const n = parseInt(readLine().trim(), 10);

    const s = readLine().replace(/\s+$/g, '').split(' ').map(sTemp => parseInt(sTemp, 10));

    const firstMultipleInput = readLine().replace(/\s+$/g, '').split(' ');

    const d = parseInt(firstMultipleInput[0], 10);

    const m = parseInt(firstMultipleInput[1], 10);

    const result = birthday(s, d, m);

    ws.write(result + '\n');

    ws.end();
}
