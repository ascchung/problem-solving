#!/bin/python3

import math
import os
import random
import re
import sys

#
# Complete the 'diagonalDifference' function below.
#
# The function is expected to return an INTEGER.
# The function accepts 2D_INTEGER_ARRAY arr as parameter.
#

def diagonalDifference(arr):
    # identify the index from each matrix row
    # add the diagonals and then subtract the difference
    # return the difference
    difference = 0
    diagonal1 = 0
    diagonal2 = 0
    n = len(arr)
    for x in range(n):
        diagonal1 += arr[x][x]
        diagonal2 += arr[x][n - x - 1]
        difference = diagonal1 - diagonal2
    return abs(difference)

if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    n = int(input().strip())

    arr = []

    for _ in range(n):
        arr.append(list(map(int, input().rstrip().split())))

    result = diagonalDifference(arr)

    fptr.write(str(result) + '\n')

    fptr.close()
