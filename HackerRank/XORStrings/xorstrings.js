process.stdin.resume();
process.stdin.setEncoding("ascii");
var input = "";

process.stdin.on("data", function (chunk) {
    input += chunk;
});

process.stdin.on("end", function () {
    const [str1, str2] = input.trim().split('\n');

    function xorBinaryStrings(s, t) {
        let result = "";
        for (let i = 0; i < s.length; i++) {
            const bit1 = s[i];
            const bit2 = t[i];
            const xorResult = (bit1 === '1' ^ bit2 === '1') ? '1' : '0';
            result += xorResult;
        }
        return result;
    }

    const xorResult = xorBinaryStrings(str1, str2);
    console.log(xorResult); // Output the XOR result directly
});
